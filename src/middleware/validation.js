const isValidPhoneNumber = (phoneNumber) => {
    const regEx = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    return regEx.test(phoneNumber);
  };
 const validateEmail = (email) => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
function validate(req, res, next) {
    if (req.originalUrl === '/auth/register') {
        const { userName, email, password, mobileNumber, profileImage } = req.body;
        if (!userName || !profileImage || !email || !password || !password.length > 5 || !mobileNumber || !isValidPhoneNumber(mobileNumber) || !validateEmail(email)) {
            return error(res);
        } 
    }
    else if (req.originalUrl === '/auth/login') {
        const { email, password} = req.body;
        if (!email || !password || !password.length > 5 || !validateEmail(email)) {
            return error(res);
        } 
    }
    else if (req.originalUrl === '/auth/profile' || req.originalUrl === '/auth/getUsers' || req.originalUrl === '/auth/getUsername' ||  req.originalUrl === '/post/friendsPosts' || req.originalUrl === '/post/myPosts') {
        const { userId} = req.body;
        if (!userId) {
            return error(res);
        }
    }
    else if (req.originalUrl === '/auth/update') {
      
        const { userName, profileImage, mobileNumber, userId} = req.body;
        if (!userName || !profileImage || !userId || !mobileNumber || !isValidPhoneNumber(mobileNumber)) {
            return error(res);
        }
    }
    else if (req.originalUrl === '/auth/follow') {
        const { userId, friendsId} = req.body;
        if (!userId || !friendsId) {
            return error(res);
        }
    }
    else if (req.originalUrl === '/post/uploadPost') {
        const { userPostTitle, userPostDescription, userId, userPostImage, userName } = req.body;
        if (!userId || !userPostTitle || !userPostDescription || !userPostImage || !userName) {
            return error(res);
        }
    }
    else if (req.originalUrl === '/post/like' || req.originalUrl === '/post/unlike') {
        const { userId, postId } = req.body;
        if (!userId || !postId) {
            return error(res);
        }
    }
    else if (req.originalUrl === '/post/addComment') {
        const { postId, userId, comments, userName, commentedAt, profileImage } = req.body;
        if (!userId || !postId || !comments || !userName || !commentedAt || !profileImage) {
            return error(res);
        }
    }
    else if (req.originalUrl === '/post/deletePosts') {
        const { postId} = req.body;
        if (!postId) {
            return error(res);
        }
    }
    else if (req.originalUrl === '/post/editPosts') {
        const { postId, userPostTitle, userPostDescription, private, userId } = req.body;
        if (!userId || !userPostTitle || !userPostDescription || !postId || !private) {
            return error(res);
        }
    }
    next();
}
function error(res) {
    return res.status(400).send({
        message: 'Bad Payload'
    });
}
module.exports = validate;