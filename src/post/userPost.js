var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
const shortid = require('shortid');
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json({ limit: "50mb" }))
router.use(bodyParser.urlencoded({ limit: "50mb", extended: true, parameterLimit: 50000 }))
var jwtToken = require('../middleware/jwtToken');
const User = require("../model/user");
const Post = require("../model/post");
var validation = require('../middleware/validation');
var response = require('../middleware/response');
//Upload Post
router.post('/uploadPost', jwtToken, validation, async (req, res) =>{
  console.log(req.body.private)
  const { userPostTitle, userPostDescription, userId, userPostImage, userName, private } = req.body;
  let postId = "123";
  const post = await new Post({
    userPostTitle,
    userPostDescription,
    userPostImage,
    postId,
    userId,
    userName,
    private
  });
  await post.save();
  post.postId = post._id
  await post.save();
  await response(res, statusCode = 200, object = {});
});
//Like Post
router.post('/like', jwtToken, validation, async (req, res) => {
  const { postId, userId } = req.body;
  const post = await Post.findOneAndUpdate(
    {
      postId: postId
    },
    { $push: { likedUsersId: userId } });
  if (post) {
    response(res, statusCode = 200, object = {});
  } else {
    response(res, statusCode = 400, 'Post cannot be liked');
  }
});
//Unlike Post
router.post('/unlike', jwtToken, validation, async (req, res) =>{
  const { postId, userId } = req.body;
  await Post.findOne({ postId: postId }, async (err, doc) => {
    if (!err) {
      var newArr = [];
      newArr = doc.likedUsersId.filter((element) => {
        return element != userId;
      });
      const post = await Post.findOneAndUpdate(
        {
          postId: postId
        },
        { $set: { likedUsersId: newArr } })
      if (post) {
        response(res, statusCode = 200, object = {});
      } else {
        response(res, statusCode = 400, 'Post cannot be unliked');
      }
    } else {
      response(res, statusCode = 400, 'Cannot find PostID');
    }
  })
});
//Get all Posts
router.get('/getPosts', jwtToken, async (req, res) =>{
  Post.find({ private: false }, (err, users)=> {
    if (!err) {
      response(res, statusCode = 200, users);
    } else {
      response(res, statusCode = 400, 'Posts cannot be retrieved');
    }
  })
});
//Add Comment to the post
router.post('/addComment', jwtToken, validation, async (req, res) => {
  const { postId, userId, comments, userName, commentedAt, profileImage } = req.body;
  const post = await Post.findOneAndUpdate(
    {
      postId: postId
    },
    {
      $push:
      {
        commentsUsersID: [{
          "userId": userId,
          "comments": comments,
          "userName": userName,
          "commentedAt": commentedAt,
          "profileImage": profileImage
        }]
      }
    }, { upsert: true, new: true });
  if (post) {
    response(res, statusCode = 200, object = {});
  } else {
    response(res, statusCode = 400, 'Posts cannot be commented');
  }
});
// Friends Posts
router.post('/friendsPosts', jwtToken, validation, async (req, res) => {
  const { userId } = req.body;
  let user = await User.findOne({
    userId
  });
  if (!user)
    response(res, statusCode = 400, "User Not Exist");
  await Post.find({ userId: user.friendsList }, (err, users) => {
    if (!err) {
      response(res, statusCode = 200, users);
    } else {
      response(res, statusCode = 400, 'Friends posts cannot be retrieved');
    }
  })
});
// My Posts
router.post('/myPosts', jwtToken, validation, async (req, res) => {
  const { userId } = req.body;
  Post.find({ userId: userId }, (err, users) =>{
    if (!err) {
      response(res, statusCode = 200, users);
    } else {
      response(res, statusCode = 400, 'Posts cannot be retrieved');
    }
  })
});
// Delete Posts
router.post('/deletePosts', jwtToken, validation, async (req, res) =>{
  const { postId } = req.body;
  const post = await Post.findOneAndRemove({ postId: postId });
  if (post) {
    response(res, statusCode = 200, object = {});
  } else {
    response(res, statusCode = 400, 'Posts cannot be deleted');
  }
});
// Edit Posts
router.post('/editPosts', jwtToken, validation, async (req, res) =>{
  const { postId, userPostTitle, userPostDescription, private, userId } = req.body;
  const query = { postId: postId };
  let user = await Post.findOneAndUpdate(query, {
    $set: {
      userPostTitle: userPostTitle,
      userPostDescription: userPostDescription,
      private: private,
      userId: userId,
    }
  });
  if (user) {
    response(res, statusCode = 200, object = {});
  }
  else {
    response(res, statusCode = 400, 'Posts cannot be updated');
  }
});
module.exports = router;
