var express = require('express');
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken')
var router = express.Router();
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json({ limit: "50mb" }))
router.use(bodyParser.urlencoded({ limit: "50mb", extended: true, parameterLimit: 50000 }))
var bcrypt = require('bcryptjs');
const User = require("../model/user");
var jwtToken = require('../middleware/jwtToken');
var validation = require('../middleware/validation');
var response = require('../middleware/response');

//Register
router.post('/register', validation, async (req, res) =>{
  const { userName, email, password, mobileNumber, profileImage } = req.body;
  let userId = "123";
  let user = await User.findOne({
    email
  });
  if (user) {
    response(res, statusCode=400, "User Already Exists");
  }
  user = new User({
    userName,
    email,
    password,
    mobileNumber,
    userId,
    profileImage
  });
  user.password = await bcrypt.hashSync(password, 10);
  await user.save();
  user.userId = user._id
  await user.save();
  const payload = {
    id: user.id
  };
  response(res, statusCode=200, payload);
});
// Login
router.post('/login', validation, async (req, res) =>{
  const { email, password } = req.body;
  const user = await User.findOne({
    email
  });
  if (!user)
  response(res, statusCode=400, "User Not Exist");
  const isMatch = await bcrypt.compareSync(password, user.password);
  if (!isMatch)
  response(res, statusCode=400, "Incorrect Password !");
  const data = {
    user: {
      id: user.userId,
      userName: user.userName,
      email: user.email,
      profileImage: user.profileImage,
      friendsList: user.friendsList
    }
  };
  const payload = {
    id: user.userId
  };
  await jwt.sign(
    payload,
    process.env.jwtSecret, {
    expiresIn: 86400000 * 15 // 15 Days
  },
    async (err, token) => {
      await res.status(200).json({
        token: token,
        message: 'Login success :)',
        data: data
      });
    }
  );
});
// View Profile
router.post('/profile', jwtToken, validation, async (req, res) =>{
  const { userId } = req.body;
  const user = await User.findOne({
    userId
  });
  if (!user)
  response(res, statusCode=400, "User Not Exist");
  const payload = {
    user: {
      id: user.userId,
      userName: user.userName,
      email: user.email,
      profileImage: user.profileImage,
      mobileNumber: user.mobileNumber,
      friendsList: user.friendsList
    }
  };
  response(res, statusCode=200, payload);
})
// Update Profile
router.post('/update', jwtToken, validation, async (req, res) =>{
  const { userName, mobileNumber, profileImage, userId } = req.body;
  const query = { userId: userId };
  const user = await User.findOneAndUpdate(query, {
    $set: {
      userName: userName,
      mobileNumber: mobileNumber,
      profileImage: profileImage
    }
  });

  if (user) {
    response(res, statusCode=200, object={});
  }
  else {
    response(res, statusCode=400, "User cannot be updated");
  }
});

// Adding Followers list 
router.post('/follow', jwtToken, validation, async (req, res) =>{
  const { friendsId, userId } = req.body;
  const user = await User.findOneAndUpdate({ userId: userId }, { $push: { friendsList: friendsId } },
    // { upsert: true, new: true }
  );
  if (user) {
    response(res, statusCode=200, object={});
  } else {
    response(res, statusCode=400,  'Friends cannot be added');
  }
});

// Getting Users List
router.post('/getUsers', jwtToken, validation, async (req, res) =>{
  const { userId } = req.body;
  let user = await User.findOne({
    userId
  });
  if (!user)
  response(res, statusCode=400, "User Not Exist");
  User.find({ userId: { $nin: user.friendsList } }, (err, users) =>{
    if (!err) {
      response(res, statusCode=200, users);
    } else {
      response(res, statusCode=400, "Friends List cannot be retrieved");
    }})
});

// Getting Usernames
router.post('/getUsername', jwtToken, validation, async (req, res) =>{
  const { userId } = req.body;
  let user = await User.find({ userId: userId });
  if (!user) {
    response(res, statusCode=400, "User Not Exist");
  }
  else {
    response(res, statusCode=200, user);
  }
});
module.exports = router;
